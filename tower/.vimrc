syntax on
set number
nnoremap <F2> :set nonumber!<CR>

set expandtab
set shiftwidth=4
set softtabstop=4
set hlsearch

highlight Visual cterm=NONE ctermbg=0 ctermfg=NONE guibg=Grey40
highlight Folded cterm=NONE ctermbg=24 ctermfg=NONE guibg=Grey40

" :copen to go to quickfix window
" :clist to list all errors
